package com.furiousomatic.app.technical.tools

import org.springframework.boot.test.util.TestPropertyValues
import org.springframework.context.ApplicationContextInitializer
import org.springframework.context.ConfigurableApplicationContext
import org.testcontainers.containers.PostgreSQLContainer
import org.testcontainers.containers.wait.strategy.Wait

class KPostgreSQLContainer(imageName: String) : PostgreSQLContainer<KPostgreSQLContainer>(imageName)

class DatabaseContainerInitializer : ApplicationContextInitializer<ConfigurableApplicationContext> {
    override fun initialize(applicationContext: ConfigurableApplicationContext) {
        dbContainer.start()
        TestPropertyValues.of(
            "spring.datasource.url=" + dbContainer.jdbcUrl,
            "spring.datasource.username=" + dbContainer.username,
            "spring.datasource.password=" + dbContainer.password,

        ).applyTo(applicationContext.environment)
    }

    companion object {
        private val dbContainer: KPostgreSQLContainer = KPostgreSQLContainer("postgres:14.1")
            .withDatabaseName("FURIOUSOMATIC_DB")
            .withUsername("FURIOUSOMATIC_DB")
            .withPassword("FURIOUSOMATIC_DB")
            .waitingFor(
                Wait.forLogMessage(".*database system is ready to accept connections.*\\n", 1)
            )
    }
}
