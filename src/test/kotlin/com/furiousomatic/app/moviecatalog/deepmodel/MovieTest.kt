package com.furiousomatic.app.moviecatalog.deepmodel

import com.furiousomatic.app.moviecatalog.tools.randomMovie
import com.furiousomatic.app.technical.tools.randomString
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import strikt.assertions.isFalse
import strikt.assertions.isTrue
import java.util.UUID
import kotlin.random.Random

class MovieTest {

    @Test
    fun `should create snapshot`() {
        // given
        val movie = Movie(
            id = UUID.randomUUID(),
            imdbId = randomString(),
            active = Random.nextBoolean(),
        )
        // when
        val snapshot = movie.toSnapshot()
        // then
        expectThat(snapshot.id).isEqualTo(movie.id)
        expectThat(snapshot.imdbId).isEqualTo(movie.imdbId)
        expectThat(snapshot.active).isEqualTo(movie.isActive())
    }

    @ParameterizedTest
    @ValueSource(booleans = [true, false])
    fun `should activate movie`(activeOnCreation: Boolean) {
        // given
        val movie = randomMovie(activeOnCreation)
        // when
        movie.activate()
        val snapshot = movie.toSnapshot()
        // then
        expectThat(snapshot.active).isTrue()
    }

    @ParameterizedTest
    @ValueSource(booleans = [true, false])
    fun `should deactivate movie`(activeOnCreation: Boolean) {
        // given
        val movie = randomMovie(activeOnCreation)
        // when
        movie.deactivate()
        val snapshot = movie.toSnapshot()
        // then
        expectThat(snapshot.active).isFalse()
    }

    @Test
    fun `should not change internal activity`() {
        // given
        val movie = Movie(
            id = UUID.randomUUID(),
            imdbId = randomString(),
            active = true,
        )
        // when
        var result = movie.isActive()
        result = false
        // then
        expectThat(movie.isActive()).isTrue()
        expectThat(result).isFalse()
    }
}
