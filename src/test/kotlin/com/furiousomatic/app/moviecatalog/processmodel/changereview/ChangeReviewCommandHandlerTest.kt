package com.furiousomatic.app.moviecatalog.processmodel.changereview

import com.furiousomatic.app.moviecatalog.deepmodel.MovieReviews
import com.furiousomatic.app.moviecatalog.deepmodel.MovieReviewsRepository
import com.furiousomatic.app.moviecatalog.deepmodel.Review
import com.furiousomatic.app.moviecatalog.deepmodel.Reviewer
import com.furiousomatic.app.usermanagement.user.CurrentUserService
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import java.util.UUID

class ChangeReviewCommandHandlerTest {
    private val movieReviewsRepository: MovieReviewsRepository = mockk()
    private val currentUserService: CurrentUserService = mockk()
    private val handler = ChangeReviewCommandHandler(movieReviewsRepository, currentUserService)

    @Test
    fun `should change review and save`() {
        // given
        val movieId = UUID.randomUUID()
        val reviewerId = UUID.randomUUID()
        val review = 4
        val command = ChangeReviewCommand(movieId, review)
        val movieReviews: MovieReviews = mockk(relaxUnitFun = true)
        every { movieReviewsRepository.findByMovieId(movieId) } returns movieReviews
        every { currentUserService.getCurrentUserId() } returns reviewerId
        every { movieReviewsRepository.save(movieReviews) } answers { }
        // when
        handler.handle(command)
        // then
        verify(exactly = 1) { movieReviewsRepository.findByMovieId(any()) }
        verify(exactly = 1) { movieReviews.changeReview(Reviewer(reviewerId), Review(review)) }
        verify(exactly = 1) { movieReviewsRepository.save(any()) }
    }
}
