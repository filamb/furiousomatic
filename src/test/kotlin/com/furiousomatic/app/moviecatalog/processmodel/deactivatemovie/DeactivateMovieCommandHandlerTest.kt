package com.furiousomatic.app.moviecatalog.processmodel.deactivatemovie

import com.furiousomatic.app.moviecatalog.deepmodel.Movie
import com.furiousomatic.app.moviecatalog.deepmodel.MovieRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import java.util.UUID

class DeactivateMovieCommandHandlerTest {
    private val movieRepository: MovieRepository = mockk()
    private val handler = DeactivateMovieCommandHandler(movieRepository)

    @Test
    fun `should deactivate and save`() {
        // given
        val id = UUID.randomUUID()
        val command = DeactivateMovieCommand(id)
        val movie: Movie = mockk(relaxUnitFun = true)
        every { movieRepository.findById(id) } returns movie
        every { movieRepository.save(movie) } answers { }
        // when
        handler.handle(command)
        // then
        verify(exactly = 1) { movieRepository.findById(any()) }
        verify(exactly = 1) { movie.deactivate() }
        verify(exactly = 1) { movieRepository.save(any()) }
    }
}
