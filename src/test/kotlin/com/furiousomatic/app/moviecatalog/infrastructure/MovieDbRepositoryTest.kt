package com.furiousomatic.app.moviecatalog.infrastructure

import com.furiousomatic.app.moviecatalog.deepmodel.Movie
import com.furiousomatic.app.moviecatalog.deepmodel.MovieNotFoundException
import com.furiousomatic.app.technical.tools.randomString
import io.mockk.every
import io.mockk.mockk
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.api.expectThrows
import strikt.assertions.isEqualTo
import java.util.Optional
import java.util.UUID

class MovieDbRepositoryTest {
    private val movieEntityRepository: MovieEntityRepository = mockk()
    private val dbMovieRepository = DbMovieRepository(movieEntityRepository)

    @Test
    fun `should get movie`() {
        // given
        val movieEntity = MovieEntity(UUID.randomUUID(), randomString(), true)
        every { movieEntityRepository.findById(movieEntity.id) } returns Optional.of(movieEntity)
        // when
        val result = dbMovieRepository.findById(movieEntity.id)
        // then
        expectThat(result.id).isEqualTo(movieEntity.id)
        expectThat(result.imdbId).isEqualTo(movieEntity.imdbId)
        expectThat(result.isActive()).isEqualTo(movieEntity.active)
        verify(exactly = 1) { movieEntityRepository.findById(any()) }
    }

    @Test
    fun `should throw exception because movie is not found`() {
        // given
        val movieId = UUID.randomUUID()
        every { movieEntityRepository.findById(movieId) } returns Optional.empty()
        // when
        expectThrows<MovieNotFoundException> { dbMovieRepository.findById(movieId) }
        // then
        verify(exactly = 1) { movieEntityRepository.findById(any()) }
    }

    @Test
    fun `should save movie`() {
        // given
        val movie = Movie(UUID.randomUUID(), randomString(), true)
        val slot = slot<MovieEntity>()
        every { movieEntityRepository.save(capture(slot)) } returnsArgument 0
        // when
        dbMovieRepository.save(movie)
        val captured = slot.captured
        // then
        expectThat(captured.id).isEqualTo(movie.id)
        expectThat(captured.imdbId).isEqualTo(movie.imdbId)
        expectThat(captured.active).isEqualTo(movie.isActive())
        verify(exactly = 1) { movieEntityRepository.save(any()) }
    }
}
