package com.furiousomatic.app.moviecatalog.infrastructure

import com.furiousomatic.app.showmanagement.tools.TransactioWrapperConfig
import com.furiousomatic.app.showmanagement.tools.TransactionWrapper
import com.furiousomatic.app.technical.JacksonConfig
import com.furiousomatic.app.technical.tools.DatabaseContainerInitializer
import com.furiousomatic.app.technical.tools.randomString
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.context.annotation.Import
import org.springframework.test.context.ContextConfiguration
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import java.util.UUID

@DataJpaTest(showSql = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(
    initializers = [DatabaseContainerInitializer::class]
)
@Import(TransactioWrapperConfig::class, JacksonConfig::class)
class MovieEntityRepositoryIntegrationTest {
    @Autowired
    private lateinit var movieRepository: MovieEntityRepository

    @Autowired
    private lateinit var transactionWrapper: TransactionWrapper

    @AfterEach
    fun cleanup() {
        movieRepository.deleteAll()
    }

    @Test
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    fun `should save and get movie`() {
        // given
        val movie = MovieEntity(UUID.randomUUID(), randomString(), true)
        // when
        transactionWrapper {
            movieRepository.save(movie)
        }
        // when
        val result = movieRepository.findById(movie.id).get()
        // then
        expectThat(result).isEqualTo(movie)
    }
}
