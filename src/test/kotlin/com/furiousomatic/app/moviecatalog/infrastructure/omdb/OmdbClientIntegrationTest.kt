package com.furiousomatic.app.moviecatalog.infrastructure.omdb

import com.furiousomatic.app.technical.JacksonConfig
import com.furiousomatic.app.technical.tools.randomString
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockserver.client.MockServerClient
import org.mockserver.junit.jupiter.MockServerExtension
import org.mockserver.junit.jupiter.MockServerSettings
import org.mockserver.model.HttpRequest
import org.mockserver.model.HttpResponse
import org.mockserver.model.MediaType
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Import
import org.springframework.test.context.TestPropertySource
import org.springframework.test.context.junit.jupiter.SpringExtension
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import strikt.assertions.isTrue

@ExtendWith(SpringExtension::class, MockServerExtension::class)
@MockServerSettings(ports = [8888])
@Import(OmdbClient::class, JacksonConfig::class)
@TestPropertySource(
    properties = [
        "furiousomatic.imdb.url=http://localhost:8888",
        "furiousomatic.imdb.api-key=test-api-key",
    ]
)
class OmdbClientIntegrationTest(private val mockServerClient: MockServerClient) {
    @Autowired
    private lateinit var omdbClient: OmdbClient

    @AfterEach
    fun afterEach() {
        mockServerClient.reset()
    }

    @Test
    fun `should return details`() {
        // given
        val movieId = randomString()
        val json = """
            {
              "Title": "The Fast and the Furious",
              "Year": "2001",
              "Rated": "PG-13",
              "Released": "22 Jun 2001",
              "Runtime": "106 min",
              "Genre": "Action, Crime, Thriller",
              "Director": "Rob Cohen",
              "Writer": "Ken Li, Gary Scott Thompson, Erik Bergquist",
              "Actors": "Vin Diesel, Paul Walker, Michelle Rodriguez",
              "Plot": "Los Angeles police officer Brian O'Conner must decide where his loyalty really lies when he becomes enamored with the street racing world he has been sent undercover to destroy.",
              "Language": "English, Spanish",
              "Country": "United States, Germany",
              "Awards": "11 wins & 18 nominations",
              "Poster": "https://m.media-amazon.com/images/M/MV5BNzlkNzVjMDMtOTdhZC00MGE1LTkxODctMzFmMjkwZmMxZjFhXkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_SX300.jpg",
              "Ratings": [
                {
                  "Source": "Internet Movie Database",
                  "Value": "6.8/10"
                },
                {
                  "Source": "Rotten Tomatoes",
                  "Value": "54%"
                },
                {
                  "Source": "Metacritic",
                  "Value": "58/100"
                }
              ],
              "Metascore": "58",
              "imdbRating": "6.8",
              "imdbVotes": "370,116",
              "imdbID": "tt0232500",
              "Type": "movie",
              "DVD": "03 Jun 2003",
              "BoxOffice": "${'$'}144,533,925",
              "Production": "N/A",
              "Website": "N/A",
              "Response": "True"
            }
        """.trimIndent()
        mockServerClient.`when`(
            HttpRequest.request()
                .withMethod("GET")
                .withPath("/")
                .withQueryStringParameter("i", movieId)
                .withQueryStringParameter("apikey", "test-api-key")
        )
            .respond(
                HttpResponse.response()
                    .withStatusCode(200)
                    .withBody(json, MediaType.APPLICATION_JSON)
            )
        // when
        val movieDetails = runBlocking {
            omdbClient.getMovieDetails(movieId)
        }.getOrThrow()
        // then
        expectThat(movieDetails.Plot).isEqualTo("Los Angeles police officer Brian O'Conner must decide where his loyalty really lies when he becomes enamored with the street racing world he has been sent undercover to destroy.")
        expectThat(movieDetails.Runtime).isEqualTo("106 min")
        expectThat(movieDetails.Runtime).isEqualTo("106 min")
        expectThat(movieDetails.Title).isEqualTo("The Fast and the Furious")
    }

    @Test
    fun `should return failure for incorrect id`() {
        // given
        val movieId = randomString()
        val error = """
            {
                "Response": "False",
                "Error": "Incorrect IMDb ID."
            }
        """.trimIndent()
        mockServerClient.`when`(
            HttpRequest.request()
                .withMethod("GET")
                .withPath("/")
                .withQueryStringParameter("i", movieId)
                .withQueryStringParameter("apikey", "test-api-key")
        )
            .respond(
                HttpResponse.response()
                    .withStatusCode(200)
                    .withBody(error, MediaType.APPLICATION_JSON)
            )
        // when
        val movieDetails = runBlocking {
            omdbClient.getMovieDetails(movieId)
        }
        // then
        expectThat(movieDetails.isFailure).isTrue()
    }

    @Test
    fun `should return failure for req error`() {
        // given
        val movieId = randomString()
        mockServerClient.`when`(
            HttpRequest.request()
                .withMethod("GET")
                .withPath("/")
                .withQueryStringParameter("i", movieId)
                .withQueryStringParameter("apikey", "test-api-key")
        )
            .respond(
                HttpResponse.response()
                    .withStatusCode(500)
            )
        // when
        val movieDetails = runBlocking {
            omdbClient.getMovieDetails(movieId)
        }
        // then
        expectThat(movieDetails.isFailure).isTrue()
    }
}
