package com.furiousomatic.app.showmanagement.processmodel.changeprice

import com.furiousomatic.app.showmanagement.deepmodel.ShowTime
import com.furiousomatic.app.showmanagement.deepmodel.ShowTimeRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.javamoney.moneta.Money
import org.junit.jupiter.api.Test
import java.util.UUID
import javax.money.Monetary

class ChangeShowPriceCommandHandlerTest {
    private val showTimeRepository: ShowTimeRepository = mockk()
    private val handler = ChangeShowPriceCommandHandler(showTimeRepository)

    @Test
    fun `should change price`() {
        // given
        val showTimeId = UUID.randomUUID()
        val showTime: ShowTime = mockk()
        val newPrice = Money.of(10, Monetary.getCurrency("USD"))
        every { showTimeRepository.findById(showTimeId) } returns showTime
        every { showTime.changePrice(newPrice) } answers {}
        every { showTimeRepository.save(showTime) } answers {}
        // when
        handler.handle(ChangeShowPriceCommand(showTimeId, newPrice))
        // then
        verify(exactly = 1) { showTimeRepository.findById(any()) }
        verify(exactly = 1) { showTime.changePrice(any()) }
        verify(exactly = 1) { showTimeRepository.save(any()) }
    }
}
