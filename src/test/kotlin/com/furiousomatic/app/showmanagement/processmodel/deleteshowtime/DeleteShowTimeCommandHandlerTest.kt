package com.furiousomatic.app.showmanagement.processmodel.deleteshowtime

import com.furiousomatic.app.showmanagement.deepmodel.ScreeningRoom
import com.furiousomatic.app.showmanagement.deepmodel.ScreeningRoomRepository
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import java.util.UUID

class DeleteShowTimeCommandHandlerTest {
    private val screeningRoomRepository: ScreeningRoomRepository = mockk()
    private val handler = DeleteShowTimeCommandHandler(screeningRoomRepository)

    @Test
    fun `should delete show time`() {
        // given
        val screeningRoom: ScreeningRoom = mockk()
        val screeningRoomId = UUID.randomUUID()
        val showTimeId = UUID.randomUUID()
        every { screeningRoomRepository.findById(screeningRoomId) } returns screeningRoom
        every { screeningRoom.deleteShowTime(showTimeId) } answers {}
        every { screeningRoomRepository.save(screeningRoom) } answers {}
        val command = DeleteShowTimeCommand(
            screeningRoomId = screeningRoomId,
            showTimeId = showTimeId
        )
        // when
        handler.handle(command)
        // then
        verify(exactly = 1) { screeningRoomRepository.findById(any()) }
        verify(exactly = 1) { screeningRoom.deleteShowTime(any()) }
        verify(exactly = 1) { screeningRoomRepository.save(any()) }
    }
}
