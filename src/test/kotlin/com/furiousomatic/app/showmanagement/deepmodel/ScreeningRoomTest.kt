package com.furiousomatic.app.showmanagement.deepmodel

import com.furiousomatic.app.showmanagement.tools.randomScreeningRoom
import com.furiousomatic.app.showmanagement.tools.randomShowTime
import io.mockk.every
import io.mockk.mockk
import org.javamoney.moneta.Money
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.api.expectThrows
import strikt.assertions.isEmpty
import strikt.assertions.isNotNull
import strikt.assertions.isNull
import java.time.Clock
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.UUID
import javax.money.Monetary

class ScreeningRoomTest {
    private val clock: Clock = mockk()

    @Test
    fun `should add new showtime`() {
        // given
        val movie = Movie(UUID.randomUUID())
        val now = Instant.now()
        val showTime = randomShowTime(movie = movie, start = now.plus(30, ChronoUnit.HOURS))
        val screeningRoom = randomScreeningRoom(clock = clock, showTimes = listOf(showTime))
        every { clock.instant() } returns now
        val newShowStart = now.plus(24, ChronoUnit.HOURS)
        val newShowEnd = now.plus(26, ChronoUnit.HOURS)
        val price = Money.of(10, Monetary.getCurrency("USD"))
        // when
        screeningRoom.addShowTime(
            movie = movie,
            showStart = newShowStart,
            showEnd = newShowEnd,
            price = price
        )
        val result = screeningRoom.getShowTimes()
        // then
        expectThat(
            result.find { it.movie == movie && it.start == newShowStart && it.end == newShowEnd }
        ).isNotNull()
    }

    @Test
    fun `should not add new showtime because it is too late`() {
        // given
        val movie = Movie(UUID.randomUUID())
        val now = Instant.now()
        val showTime = randomShowTime(movie = movie, start = now.plus(30, ChronoUnit.HOURS))
        val screeningRoom = randomScreeningRoom(clock = clock, showTimes = listOf(showTime))
        every { clock.instant() } returns now
        val newShowStart = now.plus(24, ChronoUnit.HOURS).minusSeconds(1)
        val newShowEnd = now.plus(26, ChronoUnit.HOURS)
        val price = Money.of(10, Monetary.getCurrency("USD"))
        // when
        expectThrows<TooLateOperationException> {
            screeningRoom.addShowTime(
                movie = movie,
                showStart = newShowStart,
                showEnd = newShowEnd,
                price = price
            )
        }
        val result = screeningRoom.getShowTimes()
        // then
        expectThat(
            result.find { it.movie == movie && it.start == newShowStart && it.end == newShowEnd }
        ).isNull()
    }

    @Test
    fun `should not add new showtime because new showtime overlaps another`() {
        // given
        val movie = Movie(UUID.randomUUID())
        val now = Instant.now()
        val showTime = randomShowTime(movie = movie, start = now.plus(30, ChronoUnit.HOURS))
        val screeningRoom = randomScreeningRoom(clock = clock, showTimes = listOf(showTime))
        every { clock.instant() } returns now
        val newShowStart = showTime.start.plus(1, ChronoUnit.HOURS)
        val newShowEnd = showTime.end.plus(1, ChronoUnit.HOURS)
        val price = Money.of(10, Monetary.getCurrency("USD"))
        // when
        expectThrows<ShowTimeOverlapsException> {
            screeningRoom.addShowTime(
                movie = movie,
                showStart = newShowStart,
                showEnd = newShowEnd,
                price = price
            )
        }
        val result = screeningRoom.getShowTimes()
        // then
        expectThat(
            result.find { it.movie == movie && it.start == newShowStart && it.end == newShowEnd }
        ).isNull()
    }

    @Test
    fun `should not add new showtime because start is after and`() {
        // given
        val movie = Movie(UUID.randomUUID())
        val now = Instant.now()
        val showTime = randomShowTime(movie = movie, start = now.plus(30, ChronoUnit.HOURS))
        val screeningRoom = randomScreeningRoom(clock = clock, showTimes = listOf(showTime))
        every { clock.instant() } returns now
        val newShowStart = showTime.start.plus(1, ChronoUnit.HOURS)
        val newShowEnd = showTime.end.plus(1, ChronoUnit.HOURS)
        val price = Money.of(10, Monetary.getCurrency("USD"))
        // when
        expectThrows<InvalidShowtimeDateException> {
            screeningRoom.addShowTime(
                movie = movie,
                showStart = newShowEnd,
                showEnd = newShowStart,
                price = price
            )
        }
        val result = screeningRoom.getShowTimes()
        // then
        expectThat(
            result.find { it.movie == movie && it.start == newShowStart && it.end == newShowEnd }
        ).isNull()
    }

    @Test
    fun `should remove showtime`() {
        // given
        val movie = Movie(UUID.randomUUID())
        val now = Instant.now()
        val showTime = randomShowTime(movie = movie, start = now.plus(30, ChronoUnit.HOURS))
        val screeningRoom = randomScreeningRoom(clock = clock, showTimes = listOf(showTime))
        every { clock.instant() } returns now
        // when
        screeningRoom.deleteShowTime(showTime.id)
        // then
        expectThat(screeningRoom.getShowTimes()).isEmpty()
    }

    @Test
    fun `should not remove showtime because it is too late`() {
        // given
        val movie = Movie(UUID.randomUUID())
        val now = Instant.now()
        val showTime = randomShowTime(movie = movie, start = now.plus(22, ChronoUnit.HOURS))
        val screeningRoom = randomScreeningRoom(clock = clock, showTimes = listOf(showTime))
        every { clock.instant() } returns now
        // when
        expectThrows<TooLateOperationException> { screeningRoom.deleteShowTime(showTime.id) }
        // then
        expectThat(screeningRoom.getShowTimes().find { it.id == showTime.id }).isNotNull()
    }
}
