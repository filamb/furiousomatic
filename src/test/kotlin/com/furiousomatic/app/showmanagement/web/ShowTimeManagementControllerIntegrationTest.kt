package com.furiousomatic.app.showmanagement.web

import com.furiousomatic.app.showmanagement.processmodel.addshowtime.AddShowTimeCommand
import com.furiousomatic.app.showmanagement.processmodel.addshowtime.AddShowTimeCommandHandler
import com.furiousomatic.app.showmanagement.processmodel.changeprice.ChangeShowPriceCommandHandler
import com.furiousomatic.app.showmanagement.processmodel.deleteshowtime.DeleteShowTimeCommandHandler
import com.furiousomatic.app.technical.JacksonConfig
import com.ninjasquad.springmockk.MockkBean
import io.mockk.every
import io.mockk.slot
import io.mockk.verify
import org.javamoney.moneta.Money
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.FilterType
import org.springframework.context.annotation.Import
import org.springframework.http.MediaType
import org.springframework.security.config.annotation.web.WebSecurityConfigurer
import org.springframework.test.context.junit.jupiter.SpringExtension
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.springframework.web.filter.OncePerRequestFilter
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.UUID

@ExtendWith(SpringExtension::class)
@Import(JacksonConfig::class)
@WebMvcTest(
    ShowTimeManagementController::class,
    excludeFilters = [
        ComponentScan.Filter(
            type = FilterType.ASSIGNABLE_TYPE,
            value = [WebSecurityConfigurer::class, OncePerRequestFilter::class]
        )
    ],
    excludeAutoConfiguration = [SecurityAutoConfiguration::class]
)
class ShowTimeManagementControllerIntegrationTest {
    @MockkBean
    private lateinit var addShowTimeCommandHandler: AddShowTimeCommandHandler
    @MockkBean
    private lateinit var changeShowPriceCommandHandler: ChangeShowPriceCommandHandler
    @MockkBean
    private lateinit var deleteShowTimeCommandHandler: DeleteShowTimeCommandHandler
    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    fun `should add showtime`() {
        // given
        val screeningRoomId = UUID.randomUUID()
        val movieId = UUID.randomUUID()
        val start = Instant.ofEpochMilli(1639332000000)
        val end = start.plus(2, ChronoUnit.HOURS)
        val json = """
            {
              "movieId": "$movieId",
              "start": "2021-12-12T18:00:00.000Z",
              "end": "2021-12-12T20:00:00.000Z",
              "price": 8,
              "currency": "USD"
            }
        """.trimIndent()
        val slot = slot<AddShowTimeCommand>()
        every { addShowTimeCommandHandler.handle(capture(slot)) } answers {}
        // when
        val result = mockMvc
            .perform(
                MockMvcRequestBuilders
                    .post("/api/secured/management/screening-room/$screeningRoomId/show-time")
                    .content(json)
                    .contentType(MediaType.APPLICATION_JSON)
            )
        val captured = slot.captured
        // then
        result.andExpect { status().isOk }
        verify(exactly = 1) { addShowTimeCommandHandler.handle(any()) }
        expectThat(captured.screeningRoomId).isEqualTo(screeningRoomId)
        expectThat(captured.movieId).isEqualTo(movieId)
        expectThat(captured.start).isEqualTo(start)
        expectThat(captured.end).isEqualTo(end)
        expectThat(captured.price).isEqualTo(Money.of(8, "USD"))
    }
}
