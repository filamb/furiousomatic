package com.furiousomatic.app.showmanagement.infrastructure

import com.furiousomatic.app.showmanagement.tools.TransactioWrapperConfig
import com.furiousomatic.app.showmanagement.tools.TransactionWrapper
import com.furiousomatic.app.technical.tools.DatabaseContainerInitializer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.context.annotation.Import
import org.springframework.test.context.ContextConfiguration
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import java.math.BigDecimal
import java.time.Instant
import java.util.UUID

@DataJpaTest(showSql = true)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(
    initializers = [DatabaseContainerInitializer::class]
)
@Import(TransactioWrapperConfig::class)
class ShowTimeEntityRepositoryIntegrationTest {
    @Autowired
    private lateinit var showTimeRepository: ShowTimeEntityRepository
    @Autowired
    private lateinit var screeningRoomRepository: ScreeningRoomEntityRepository
    @Autowired
    private lateinit var transactionWrapper: TransactionWrapper

    @AfterEach
    fun cleanup() {
        showTimeRepository.deleteAll()
        screeningRoomRepository.deleteAll()
    }

    @Test
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    fun `should save and get showtime`() {
        // given
        val screeningRoom = ScreeningRoomEntity(UUID.randomUUID(), emptyList())
        val showTime = ShowTimeEntity(
            id = UUID.randomUUID(),
            screeningRoomId = screeningRoom.id,
            movieId = UUID.randomUUID(),
            start = Instant.now(),
            end = Instant.now().plusSeconds(1234),
            price = BigDecimal.valueOf(123),
            currency = "USD"
        )
        // when
        transactionWrapper {
            screeningRoomRepository.save(screeningRoom)
            showTimeRepository.save(showTime)
        }
        val result = showTimeRepository.findById(showTime.id).get()
        // then
        expectThat(result).isEqualTo(showTime)
    }
}
