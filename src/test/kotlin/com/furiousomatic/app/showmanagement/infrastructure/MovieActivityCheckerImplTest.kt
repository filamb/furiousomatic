package com.furiousomatic.app.showmanagement.infrastructure

import com.furiousomatic.app.moviecatalog.querymodel.movieactivity.MovieActivity
import com.furiousomatic.app.moviecatalog.querymodel.movieactivity.MovieActivityFetcher
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import strikt.assertions.isFalse
import java.util.UUID
import kotlin.random.Random.Default.nextBoolean

class MovieActivityCheckerImplTest {
    private val movieActivityFinder: MovieActivityFetcher = mockk()
    private val checker = MovieActivityCheckerImpl(movieActivityFinder)

    @Test
    fun `should return activity`() {
        // given
        val movieActivity = MovieActivity(
            UUID.randomUUID(),
            nextBoolean()
        )
        every { movieActivityFinder.findByMovieId(movieActivity.movieId) } returns movieActivity
        // when
        val result = checker.isActive(movieActivity.movieId)
        // then
        expectThat(result).isEqualTo(movieActivity.active)
        verify(exactly = 1) { movieActivityFinder.findByMovieId(any()) }
    }

    @Test
    fun `should return false because of exception`() {
        // given
        val movieId = UUID.randomUUID()
        every { movieActivityFinder.findByMovieId(movieId) } throws IllegalStateException()
        // when
        val result = checker.isActive(movieId)
        // then
        expectThat(result).isFalse()
        verify(exactly = 1) { movieActivityFinder.findByMovieId(any()) }
    }
}
