package com.furiousomatic.app.showmanagement.infrastructure

import io.mockk.mockk
import org.junit.Ignore
import org.junit.jupiter.api.Test
import java.time.Clock

class ShowTimesDbRepositoryTest {
    private val showTimeEntityRepository: ShowTimeEntityRepository = mockk()
    private val clock: Clock = mockk()
    private val screeningRoomDbRepository = ShowTimeDbRepository(showTimeEntityRepository, clock)

    @Ignore
    @Test
    fun `should get show time`() {
        // TODO
    }

    @Ignore
    @Test
    fun `should throw exception for not existing show time`() {
        // TODO
    }

    @Ignore
    @Test
    fun `should save show time`() {
        // TODO
    }
}
