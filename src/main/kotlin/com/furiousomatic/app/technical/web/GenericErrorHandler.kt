package com.furiousomatic.app.technical.web

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class GenericErrorHandler {

    @ExceptionHandler(Exception::class)
    fun handleGenericException(ex: Exception): ResponseEntity<ErrorDto> {
        return ResponseEntity(
            ErrorDto(ex.message),
            HttpStatus.INTERNAL_SERVER_ERROR
        )
    }
}

data class ErrorDto(val message: String?)
