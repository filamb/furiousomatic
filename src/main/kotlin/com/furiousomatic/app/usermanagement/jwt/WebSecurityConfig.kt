package com.furiousomatic.app.usermanagement.jwt

import com.furiousomatic.app.technical.ApiConstants
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.config.http.SessionCreationPolicy
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
internal class WebSecurityConfig(
    private val jwtAuthenticationEntryPoint: JwtAuthenticationEntryPoint,
    private val jwtUserDetailsService: JwtUserDetailsService,
    private val accessTokenPreAuthorizationFilter: AccessTokenPreAuthorizationFilter,
) : WebSecurityConfigurerAdapter() {

    @Autowired
    fun configureGlobal(passwordEncoder: PasswordEncoder, auth: AuthenticationManagerBuilder) {
        auth.userDetailsService(jwtUserDetailsService).passwordEncoder(passwordEncoder)
    }

    @Bean
    override fun authenticationManagerBean(): AuthenticationManager {
        return super.authenticationManagerBean()
    }

    override fun configure(httpSecurity: HttpSecurity) {
        httpSecurity
            // Add a filter to validate the tokens with every request
            .addFilterBefore(accessTokenPreAuthorizationFilter, UsernamePasswordAuthenticationFilter::class.java)
            .authorizeRequests().antMatchers("${ApiConstants.PUBLIC}/**").permitAll()
            .and()
            .authorizeRequests().antMatchers("${ApiConstants.SECURED}/**").authenticated()
            .and()
            .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
            // make sure we use stateless session; session won't be used to store user's state.
            .and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
            .and()
            .csrf().disable()
    }
}
