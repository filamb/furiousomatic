package com.furiousomatic.app.usermanagement.jwt

import com.furiousomatic.app.technical.ApiConstants
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.security.authentication.AuthenticationManager
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@CrossOrigin
internal class JwtAuthenticationController(
    private val authenticationManager: AuthenticationManager,
    private val accessTokenProvider: AccessTokenProvider,
    private val userDetailsService: JwtUserDetailsService,
) : JwtAuthenticationApiDocs {

    @RequestMapping(value = ["${ApiConstants.PUBLIC}/authenticate"], method = [RequestMethod.POST])
    override fun createAuthenticationToken(@RequestBody authenticationRequest: JwtRequest): ResponseEntity<JwtTokenResponse> {
        return runCatching {
            authenticationManager.authenticate(
                UsernamePasswordAuthenticationToken(
                    authenticationRequest.username,
                    authenticationRequest.password
                )
            )
            val userDetails = userDetailsService.loadUserByUsername(authenticationRequest.username)
            accessTokenProvider.getAccessToken(userDetails)
        }.fold(
            onSuccess = { token ->
                ResponseEntity.ok(JwtTokenResponse(token))
            },
            onFailure = {
                ResponseEntity.status(HttpStatus.UNAUTHORIZED).build()
            }
        )
    }
}

data class JwtTokenResponse(val token: String)
