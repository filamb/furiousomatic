package com.furiousomatic.app.usermanagement.jwt

import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.stereotype.Component
import org.springframework.web.filter.OncePerRequestFilter
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

@Component
class AccessTokenPreAuthorizationFilter(private val accessTokenProvider: AccessTokenProvider) : OncePerRequestFilter() {

    override fun doFilterInternal(
        request: HttpServletRequest,
        response: HttpServletResponse,
        filterChain: FilterChain
    ) {
        val header = request.getHeader("Authorization")
        val token = getTokenFromHeader(header)
        if (token?.isNotBlank() == true) {
            runCatching {
                SecurityContextHolder.getContext().authentication = accessTokenProvider.extractAuthentication(token)
            }.onFailure {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Jwt token is invalid or incorrect")
            }
        }
        filterChain.doFilter(request, response)
    }

    fun getTokenFromHeader(header: String?): String? {
        return if (header != null && header.startsWith("Bearer ")) {
            header.substring(7)
        } else {
            null
        }
    }
}
