package com.furiousomatic.app.showmanagement.deepmodel

import java.util.UUID

internal interface ShowTimeRepository {
    fun findById(id: UUID): ShowTime
    fun save(showTime: ShowTime)
}
