package com.furiousomatic.app.showmanagement.deepmodel

import java.util.UUID

interface MovieActivityChecker {
    fun isActive(movieId: UUID): Boolean
}
