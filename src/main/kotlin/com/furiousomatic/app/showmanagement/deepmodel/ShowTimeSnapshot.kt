package com.furiousomatic.app.showmanagement.deepmodel

import java.math.BigDecimal
import java.time.Instant
import java.util.UUID
import javax.money.MonetaryAmount

internal data class ShowTimeSnapshot(
    val id: UUID,
    val screeningRoomId: UUID,
    val movie: Movie,
    val start: Instant,
    val end: Instant,
    val price: MonetaryAmount
) {
    fun getPriceAmount(): BigDecimal {
        return this.price.number.numberValue(BigDecimal::class.java)
    }

    fun getCurrency(): String {
        return this.price.currency.currencyCode
    }
}
