package com.furiousomatic.app.showmanagement.deepmodel

import com.furiousomatic.app.showmanagement.shared.FULL_DAY
import java.time.Clock
import java.time.Duration
import java.time.Instant
import java.util.UUID
import javax.money.MonetaryAmount

internal class ScreeningRoom(
    val id: UUID,
    private val clock: Clock,
    showTimes: List<ShowTime>
) {
    private val showTimes: MutableList<ShowTime> = showTimes.toMutableList()

    fun addShowTime(movie: Movie, showStart: Instant, showEnd: Instant, price: MonetaryAmount) {
        val durationToShow = Duration.between(clock.instant(), showStart)
        if (durationToShow < FULL_DAY) {
            throw TooLateOperationException(FULL_DAY)
        }
        if (showStart.isAfter(showEnd)) {
            throw InvalidShowtimeDateException(showStart, showEnd)
        }
        val newShowTime = ShowTime(
            id = UUID.randomUUID(),
            screeningRoomId = id,
            movie = movie,
            start = showStart,
            end = showEnd,
            price = price,
            clock = clock
        )
        if (showTimes.any { it.overlaps(newShowTime) }) {
            throw ShowTimeOverlapsException(id, showStart, showEnd)
        } else {
            showTimes.add(newShowTime)
        }
    }

    fun getShowTimes(): List<ShowTimeSnapshot> {
        return showTimes.map { it.toSnapshot() }
    }

    fun deleteShowTime(showTimeId: UUID) {
        showTimes.find { it.id == showTimeId }?.let { showTime ->
            val durationToShow = Duration.between(clock.instant(), showTime.start)
            if (durationToShow < FULL_DAY) {
                throw TooLateOperationException(FULL_DAY)
            }
            showTimes.removeIf { it.id == showTimeId }
        } ?: throw ShowTimeNotFoundException(showTimeId)
    }
}
