package com.furiousomatic.app.showmanagement.deepmodel

import java.util.UUID

internal interface ScreeningRoomRepository {
    fun findById(id: UUID): ScreeningRoom
    fun save(screeningRoom: ScreeningRoom)
}
