package com.furiousomatic.app.showmanagement.deepmodel

import java.util.UUID

internal data class Movie(val id: UUID)
