package com.furiousomatic.app.showmanagement.web

import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import org.springframework.web.bind.annotation.RequestBody
import java.util.UUID

@Api(description = "Show Times Management", tags = ["Show Times Management"])
interface ShowTimeManagementApiDocs {

    @ApiOperation("Add show time to screening room", notes = "For logged admin")
    @ApiImplicitParam(
        name = "Authorization",
        value = "Access Token",
        required = true,
        allowEmptyValue = false,
        paramType = "header",
        dataTypeClass = String::class,
        example = "Bearer access_token"
    )
    fun addShowTime(
        @ApiParam(
            value = "Screening Room id",
            example = "fb9f8757-4c3f-40a7-b03d-ec2e70f30344",
            required = true
        ) screeningRoomId: UUID,
        request: AddShowTimeRequest
    )

    @ApiOperation("Change show time price", notes = "For logged admin")
    @ApiImplicitParam(
        name = "Authorization",
        value = "Access Token",
        required = true,
        allowEmptyValue = false,
        paramType = "header",
        dataTypeClass = String::class,
        example = "Bearer access_token"
    )
    fun changeShowTimePrice(
        @ApiParam(
            value = "Screening Room id",
            example = "fb9f8757-4c3f-40a7-b03d-ec2e70f30344",
            required = true
        ) screeningRoomId: UUID,
        @ApiParam(
            value = "Show Time Id",
            example = "6eef3fe1-a684-4e77-8312-041753ab2d80",
            required = true
        ) showTimeId: UUID,
        @RequestBody request: ChangeShowPriceRequest
    )

    @ApiOperation("Delete showtime", notes = "For logged admin")
    @ApiImplicitParam(
        name = "Authorization",
        value = "Access Token",
        required = true,
        allowEmptyValue = false,
        paramType = "header",
        dataTypeClass = String::class,
        example = "Bearer access_token"
    )
    fun deleteShowTime(
        @ApiParam(
            value = "Screening Room id",
            example = "fb9f8757-4c3f-40a7-b03d-ec2e70f30344",
            required = true
        ) screeningRoomId: UUID,
        @ApiParam(
            value = "Show Time Id",
            example = "6eef3fe1-a684-4e77-8312-041753ab2d80",
            required = true
        ) showTimeId: UUID
    )
}
