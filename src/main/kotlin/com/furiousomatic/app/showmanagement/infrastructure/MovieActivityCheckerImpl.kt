package com.furiousomatic.app.showmanagement.infrastructure

import com.furiousomatic.app.moviecatalog.querymodel.movieactivity.MovieActivityFetcher
import com.furiousomatic.app.showmanagement.deepmodel.MovieActivityChecker
import com.furiousomatic.app.technical.logger
import org.springframework.stereotype.Component
import java.util.UUID

@Component
internal class MovieActivityCheckerImpl(private val movieActivityFinder: MovieActivityFetcher) : MovieActivityChecker {
    private val log by logger()
    override fun isActive(movieId: UUID): Boolean {
        return runCatching {
            movieActivityFinder.findByMovieId(movieId)
        }.fold(
            onSuccess = { it.active },
            onFailure = {
                log.error("Could not get movie activity for id: $movieId", it)
                false
            }
        )
    }
}
