package com.furiousomatic.app.showmanagement.infrastructure

import com.furiousomatic.app.showmanagement.querymodel.ShowTimeDto
import com.furiousomatic.app.showmanagement.querymodel.ShowTimesFetcher
import org.springframework.stereotype.Component
import java.util.UUID

@Component
internal class ShowTimesFetcherImpl(
    private val showTimeEntityRepository: ShowTimeEntityRepository
) : ShowTimesFetcher {
    override fun getForMovie(movieId: UUID): List<ShowTimeDto> {
        return showTimeEntityRepository.findByMovieId(movieId).map {
            ShowTimeDto(
                id = it.id,
                start = it.start,
                end = it.end,
                price = it.price,
                currency = it.currency
            )
        }
    }
}
