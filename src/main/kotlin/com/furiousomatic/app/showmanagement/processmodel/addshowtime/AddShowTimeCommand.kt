package com.furiousomatic.app.showmanagement.processmodel.addshowtime

import com.furiousomatic.app.technical.Command
import java.time.Instant
import java.util.UUID
import javax.money.MonetaryAmount

data class AddShowTimeCommand(
    val screeningRoomId: UUID,
    val movieId: UUID,
    val start: Instant,
    val end: Instant,
    val price: MonetaryAmount
) : Command
