package com.furiousomatic.app.showmanagement.processmodel.changeprice

import com.furiousomatic.app.showmanagement.deepmodel.ShowTimeRepository
import com.furiousomatic.app.technical.CommandHandler
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
internal class ChangeShowPriceCommandHandler(
    private val showTimeRepository: ShowTimeRepository
) : CommandHandler<ChangeShowPriceCommand, Unit> {

    @PreAuthorize("hasRole('ADMIN')")
    @Transactional
    override fun handle(command: ChangeShowPriceCommand) {
        val showTime = showTimeRepository.findById(command.showTimeId)
        showTime.changePrice(command.price)
        showTimeRepository.save(showTime)
    }
}
