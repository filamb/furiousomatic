package com.furiousomatic.app.showmanagement.processmodel.deleteshowtime

import com.furiousomatic.app.technical.Command
import java.util.UUID

data class DeleteShowTimeCommand(
    val screeningRoomId: UUID,
    val showTimeId: UUID
) : Command
