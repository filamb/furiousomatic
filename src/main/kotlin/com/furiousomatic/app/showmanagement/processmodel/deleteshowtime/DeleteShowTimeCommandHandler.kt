package com.furiousomatic.app.showmanagement.processmodel.deleteshowtime

import com.furiousomatic.app.showmanagement.deepmodel.ScreeningRoomRepository
import com.furiousomatic.app.technical.CommandHandler
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
internal class DeleteShowTimeCommandHandler(
    private val screeningRoomRepository: ScreeningRoomRepository
) : CommandHandler<DeleteShowTimeCommand, Unit> {

    @PreAuthorize("hasRole('ADMIN')")
    @Transactional
    override fun handle(command: DeleteShowTimeCommand) {
        val screeningRoom = screeningRoomRepository.findById(command.screeningRoomId)
        screeningRoom.deleteShowTime(command.showTimeId)
        screeningRoomRepository.save(screeningRoom)
    }
}
