package com.furiousomatic.app.showmanagement.shared

import java.time.Duration

val FULL_DAY: Duration = Duration.ofHours(24)
