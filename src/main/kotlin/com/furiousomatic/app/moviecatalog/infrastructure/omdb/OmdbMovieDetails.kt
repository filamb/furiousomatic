package com.furiousomatic.app.moviecatalog.infrastructure.omdb

data class OmdbMovieDetails(
    val Title: String,
    val Released: String,
    val Runtime: String,
    val Plot: String
)
