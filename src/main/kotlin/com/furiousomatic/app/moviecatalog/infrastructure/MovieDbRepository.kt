package com.furiousomatic.app.moviecatalog.infrastructure

import com.furiousomatic.app.moviecatalog.deepmodel.Movie
import com.furiousomatic.app.moviecatalog.deepmodel.MovieNotFoundException
import com.furiousomatic.app.moviecatalog.deepmodel.MovieRepository
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Repository
internal class DbMovieRepository(private val movieEntityRepository: MovieEntityRepository) : MovieRepository {
    override fun findById(id: UUID): Movie {
        return movieEntityRepository.findById(id)
            .map { Movie(it.id, it.imdbId, it.active) }
            .orElseThrow { MovieNotFoundException(id) }
    }

    @Transactional(propagation = Propagation.MANDATORY)
    override fun save(movie: Movie) {
        val snapshot = movie.toSnapshot()
        val entity = MovieEntity(snapshot.id, snapshot.imdbId, snapshot.active)
        movieEntityRepository.save(entity)
    }
}

@Table(name = "MC_MOVIE")
@Entity
internal data class MovieEntity(
    @Id
    @Column(name = "ID")
    val id: UUID,
    @Column(name = "IMDB_ID")
    val imdbId: String,
    @Column(name = "ACTIVE")
    val active: Boolean
)

internal interface MovieEntityRepository : CrudRepository<MovieEntity, UUID>
