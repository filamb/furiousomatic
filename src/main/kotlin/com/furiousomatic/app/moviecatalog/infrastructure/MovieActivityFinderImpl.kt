package com.furiousomatic.app.moviecatalog.infrastructure

import com.furiousomatic.app.moviecatalog.deepmodel.MovieRepository
import com.furiousomatic.app.moviecatalog.deepmodel.MovieSnapshot
import com.furiousomatic.app.moviecatalog.querymodel.movieactivity.MovieActivity
import com.furiousomatic.app.moviecatalog.querymodel.movieactivity.MovieActivityFetcher
import org.springframework.stereotype.Service
import java.util.UUID

@Service
internal class MovieActivityFinderImpl(private val movieRepository: MovieRepository) : MovieActivityFetcher {
    override fun findByMovieId(movieId: UUID): MovieActivity {
        return movieRepository.findById(movieId).toSnapshot().toMovieActivity()
    }
}

private fun MovieSnapshot.toMovieActivity(): MovieActivity {
    return MovieActivity(this.id, this.active)
}
