package com.furiousomatic.app.moviecatalog.infrastructure.omdb

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding

@ConfigurationProperties("furiousomatic.imdb")
@ConstructorBinding
data class OmdbClientProperties(
    val url: String,
    val apiKey: String
)
