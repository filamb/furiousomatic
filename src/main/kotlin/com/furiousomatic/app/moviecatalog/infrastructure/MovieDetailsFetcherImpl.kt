package com.furiousomatic.app.moviecatalog.infrastructure

import com.furiousomatic.app.moviecatalog.deepmodel.MovieRepository
import com.furiousomatic.app.moviecatalog.infrastructure.omdb.OmdbClient
import com.furiousomatic.app.moviecatalog.querymodel.moviedetails.MovieDetails
import com.furiousomatic.app.moviecatalog.querymodel.moviedetails.MovieDetailsFetcher
import com.furiousomatic.app.moviecatalog.querymodel.moviedetails.MovieDetailsNotAvailableException
import kotlinx.coroutines.runBlocking
import org.springframework.stereotype.Component
import java.util.UUID

@Component
internal class MovieDetailsFetcherImpl(
    private val movieRepository: MovieRepository,
    private val omdbClient: OmdbClient
) : MovieDetailsFetcher {
    override fun getByMovieId(movieId: UUID): MovieDetails {
        val movie = movieRepository.findById(movieId)
        return runBlocking {
            omdbClient.getMovieDetails(movie.imdbId)
        }
            .fold(
                onSuccess = {
                    MovieDetails(
                        name = it.Title,
                        description = it.Plot,
                        releaseDate = it.Released,
                        runtime = it.Runtime
                    )
                },
                onFailure = {
                    throw MovieDetailsNotAvailableException(movieId)
                }
            )
    }
}
