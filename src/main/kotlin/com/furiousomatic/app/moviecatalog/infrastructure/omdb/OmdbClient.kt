package com.furiousomatic.app.moviecatalog.infrastructure.omdb

import com.fasterxml.jackson.databind.ObjectMapper
import com.furiousomatic.app.technical.logger
import io.ktor.client.HttpClient
import io.ktor.client.call.receive
import io.ktor.client.engine.cio.CIO
import io.ktor.client.features.HttpTimeout
import io.ktor.client.features.defaultRequest
import io.ktor.client.features.json.JacksonSerializer
import io.ktor.client.features.json.JsonFeature
import io.ktor.client.features.logging.LogLevel
import io.ktor.client.features.logging.Logging
import io.ktor.client.request.get
import io.ktor.client.request.parameter
import io.ktor.client.statement.HttpResponse
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.stereotype.Component
import java.time.Duration

@Component
@EnableConfigurationProperties(OmdbClientProperties::class)
internal class OmdbClient(
    private val objectMapper: ObjectMapper,
    private val properties: OmdbClientProperties
) {
    private val log by logger()
    private val client = HttpClient(CIO) {
        install(Logging) {
            level = LogLevel.NONE
        }
        install(HttpTimeout) {
            requestTimeoutMillis = Duration.ofSeconds(15).toMillis()
        }
        install(JsonFeature) {
            serializer = JacksonSerializer(objectMapper)
        }
        defaultRequest {
            parameter("apikey", properties.apiKey)
        }
    }

    init {
        log.info("Omdb client base url: ${properties.url}")
    }

    suspend fun getMovieDetails(imdbId: String): Result<OmdbMovieDetails> {
        return runCatching<OmdbClient, OmdbMovieDetails> {
            val response: HttpResponse = client.get("${properties.url}/?i=$imdbId")
            response.receive()
        }.onFailure { log.error("Error when fetching movie details form omdb client. Id: $imdbId", it) }
    }
}
