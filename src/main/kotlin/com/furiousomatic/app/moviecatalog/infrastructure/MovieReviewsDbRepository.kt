package com.furiousomatic.app.moviecatalog.infrastructure

import com.furiousomatic.app.moviecatalog.deepmodel.MovieReviews
import com.furiousomatic.app.moviecatalog.deepmodel.MovieReviewsRepository
import com.furiousomatic.app.moviecatalog.deepmodel.Review
import com.vladmihalcea.hibernate.type.json.JsonBinaryType
import org.hibernate.annotations.Type
import org.hibernate.annotations.TypeDef
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository
import org.springframework.transaction.annotation.Propagation
import org.springframework.transaction.annotation.Transactional
import java.util.Optional
import java.util.UUID
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Repository
internal class DbMovieReviewsRepository(private val movieReviewsEntityRepository: MovieReviewsEntityRepository) :
    MovieReviewsRepository {
    override fun findByMovieId(movieId: UUID): MovieReviews {
        val results = movieReviewsEntityRepository.findByMovieId(movieId)
            .map { it.reviews.reviews }
            .orElse(emptyMap())
        return MovieReviews(
            movieId,
            results.mapValues { Review(it.value.rating) }
        )
    }

    @Transactional(propagation = Propagation.MANDATORY)
    override fun save(movieReviews: MovieReviews) {
        val (movieId, reviewByReviewerId) = movieReviews.toSnapshot()
        val entity = MovieReviewsEntity(
            movieId,
            ReviewsCol(
                reviewByReviewerId.mapValues { ReviewColData(it.value.rating) }
            )
        )
        movieReviewsEntityRepository.save(entity)
    }
}

@Table(name = "MC_MOVIE_REVIEWS")
@Entity
@TypeDef(name = "jsonb", typeClass = JsonBinaryType::class)
internal data class MovieReviewsEntity(
    @Id
    @Column(name = "MOVIE_ID")
    val movieId: UUID,
    @Type(type = "jsonb")
    @Column(name = "REVIEWS", columnDefinition = "jsonb")
    val reviews: ReviewsCol
)

internal data class ReviewsCol(
    val reviews: Map<UUID, ReviewColData>
)

internal data class ReviewColData(
    val rating: Int
)

internal interface MovieReviewsEntityRepository : CrudRepository<MovieReviewsEntity, UUID> {
    fun findByMovieId(movieId: UUID): Optional<MovieReviewsEntity>
}
