package com.furiousomatic.app.moviecatalog.processmodel.changereview

import com.furiousomatic.app.moviecatalog.deepmodel.MovieReviewsRepository
import com.furiousomatic.app.moviecatalog.deepmodel.Review
import com.furiousomatic.app.moviecatalog.deepmodel.Reviewer
import com.furiousomatic.app.technical.CommandHandler
import com.furiousomatic.app.usermanagement.user.CurrentUserService
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
internal class ChangeReviewCommandHandler(
    private val movieReviewsRepository: MovieReviewsRepository,
    private val currentUserService: CurrentUserService
) : CommandHandler<ChangeReviewCommand, Unit> {

    @PreAuthorize("hasRole('CUSTOMER')")
    @Transactional
    override fun handle(command: ChangeReviewCommand) {
        val movie = movieReviewsRepository.findByMovieId(command.movieId)
        movie.changeReview(Reviewer(currentUserService.getCurrentUserId()), Review(command.rating))
        movieReviewsRepository.save(movie)
    }
}
