package com.furiousomatic.app.moviecatalog.processmodel.activatemovie

import com.furiousomatic.app.moviecatalog.deepmodel.MovieRepository
import com.furiousomatic.app.technical.CommandHandler
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Component
import org.springframework.transaction.annotation.Transactional

@Component
internal class ActivateMovieCommandHandler(private val movieRepository: MovieRepository) :
    CommandHandler<ActivateMovieCommand, Unit> {
    @PreAuthorize("hasRole('ADMIN')")
    @Transactional
    override fun handle(command: ActivateMovieCommand) {
        val movie = movieRepository.findById(command.movieId)
        movie.activate()
        movieRepository.save(movie)
    }
}
