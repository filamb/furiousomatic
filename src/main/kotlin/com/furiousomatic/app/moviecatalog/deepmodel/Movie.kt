package com.furiousomatic.app.moviecatalog.deepmodel

import java.util.UUID

internal class Movie(
    val id: UUID,
    val imdbId: String,
    active: Boolean
) {
    private var _active = active

    fun activate() {
        _active = true
    }

    fun deactivate() {
        _active = false
    }

    fun isActive(): Boolean {
        return _active
    }

    fun toSnapshot(): MovieSnapshot {
        return MovieSnapshot(id, imdbId, _active)
    }
}
