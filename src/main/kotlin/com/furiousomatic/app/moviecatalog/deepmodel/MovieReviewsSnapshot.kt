package com.furiousomatic.app.moviecatalog.deepmodel

import java.util.UUID

data class MovieReviewsSnapshot(
    val movieId: UUID,
    val reviewByReviewerId: Map<UUID, Review>
)
