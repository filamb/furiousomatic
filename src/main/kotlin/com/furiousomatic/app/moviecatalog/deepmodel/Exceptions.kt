package com.furiousomatic.app.moviecatalog.deepmodel

import java.util.UUID

sealed class MovieCatalogException(message: String?, cause: Throwable? = null) : RuntimeException(message, cause)

class MovieNotFoundException(movieId: UUID) : MovieCatalogException("Could not find movie for id: $movieId")

class ReviewAlreadyExistsException(movieId: UUID, reviewerId: UUID) :
    MovieCatalogException("Review already exists for movie: $movieId and reviewer: $reviewerId")

class ReviewNotFoundException(movieId: UUID, reviewerId: UUID) :
    MovieCatalogException("Review not found for movie: $movieId and reviewer: $reviewerId")

class InvalidReviewRatingException(rating: Review) :
    MovieCatalogException("Rating for movie must be between 1 and 5. Current review: $rating")
