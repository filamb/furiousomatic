package com.furiousomatic.app.moviecatalog.web

import io.swagger.annotations.Api
import io.swagger.annotations.ApiImplicitParam
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiParam
import java.util.UUID

@Api(description = "Movie Review", tags = ["Movie Review"])
interface MovieReviewApiDocs {
    @ApiOperation("Change movie review", notes = "For logged customers")
    @ApiImplicitParam(
        name = "Authorization",
        value = "Access Token",
        required = true,
        allowEmptyValue = false,
        paramType = "header",
        dataTypeClass = String::class,
        example = "Bearer access_token"
    )
    fun changeReview(
        @ApiParam(
            value = "Movie Id",
            example = "68c5970c-92cf-4083-9008-207a691c6ab9",
            required = true
        ) movieId: UUID,
        request: ReviewRequest
    )

    @ApiOperation("Add movie review", notes = "For logged customers")
    @ApiImplicitParam(
        name = "Authorization",
        value = "Access Token",
        required = true,
        allowEmptyValue = false,
        paramType = "header",
        dataTypeClass = String::class,
        example = "Bearer access_token"
    )
    fun addReview(
        @ApiParam(
            value = "Movie Id",
            example = "68c5970c-92cf-4083-9008-207a691c6ab9",
            required = true
        ) movieId: UUID,
        request: ReviewRequest
    )
}
