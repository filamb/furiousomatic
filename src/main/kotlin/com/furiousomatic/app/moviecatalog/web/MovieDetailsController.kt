package com.furiousomatic.app.moviecatalog.web

import com.furiousomatic.app.moviecatalog.querymodel.moviedetails.MovieDetails
import com.furiousomatic.app.moviecatalog.querymodel.moviedetails.MovieDetailsFetcher
import com.furiousomatic.app.technical.ApiConstants
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.util.UUID

@RestController
@RequestMapping("${ApiConstants.PUBLIC}/movie/{movieId}/details")
class MovieDetailsController(private val movieDetailsFetcher: MovieDetailsFetcher) : MovieDetailsApiDocs {

    @GetMapping
    override fun getMovieDetails(@PathVariable("movieId") movieId: UUID): MovieDetails {
        return movieDetailsFetcher.getByMovieId(movieId)
    }
}
