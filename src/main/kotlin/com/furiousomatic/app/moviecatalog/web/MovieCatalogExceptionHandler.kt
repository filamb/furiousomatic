package com.furiousomatic.app.moviecatalog.web

import com.furiousomatic.app.moviecatalog.deepmodel.MovieNotFoundException
import com.furiousomatic.app.moviecatalog.querymodel.moviedetails.MovieDetailsNotAvailableException
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class MovieCatalogExceptionHandler {

    @ExceptionHandler(MovieDetailsNotAvailableException::class)
    fun handleMovieDetailsNotAvailableException(): ResponseEntity<Unit> {
        return ResponseEntity.internalServerError().build()
    }

    @ExceptionHandler(MovieNotFoundException::class)
    fun handleMovieNotFoundException(): ResponseEntity<Unit> {
        return ResponseEntity.notFound().build()
    }
}
