package com.furiousomatic.app.moviecatalog.querymodel.movieactivity

import java.util.UUID

data class MovieActivity(
    val movieId: UUID,
    val active: Boolean
)
