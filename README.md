# Furiousomatic

This is app for running small and simple cinema. App is mainly using:
* Kotlin
* Spring Boot
* PostgreSQL
* Maven

Some of additional libs:
* ktor client - http client
* Strikt - for test assertions
* Mockk and mockserver - for test mocks
* Flyway - for database management
* Moneta [Java Money] - for money calculations

## How to run this app locally:
1. Install Java 11
2. Run ```./rebuild-app.sh``` script to build app. That includes cleaning, testing and building app.
3. Install docker-compose
4. Run ```docker-compose up -d``` to run container with PostgreSQL database
5. Run ```./run-app.sh``` command to run app with properties for local env
6. Run ```./init-db.sh``` command to initialize local database

Application is using env variables to configure each environment. To check, which variables are used please take a look into ```/src/main/resources/application.yaml```.

For local development, all needed variables are set in ```run-app.sh```script

## API

Currently, app exposes an API for:
* Authentication
* Getting movie details
* Getting show-times for a movie
* Reviewing movie - SECURED - only for users with CUSTOMER role
* Managing show-times: adding/deleting/changing show-time price - SECURED - only for users with ADMIN role

Application is stateless and is secured by JWT token. For secured endpoints, requests should include Authorization header with Bearer token

API documentation is exposed by swagger-ui and can be checked locally using http://localhost:8080/swagger-ui/index.html#/

App currently enables to log in as one out of two (mocked - check ```JwtUserDetailsService```) users:
1. **Admin**
   * user: ADMIN
   * password: ADMIN
   * role: ADMIN
2. **Customer**
    * user: CUSTOMER
    * password: CUSTOMER
    * role: CUSTOMER

Script ``init-db.sh`` includes some hard-coded uuids that can be used to easily go through the API. I suggest to use:
* Movie Id - **68c5970c-92cf-4083-9008-207a691c6ab9** - that is the latest F&F movie - F9
* Screening Room Id - **fb9f8757-4c3f-40a7-b03d-ec2e70f30344**
* Show Time Id - **6eef3fe1-a684-4e77-8312-041753ab2d80**

# Analysis and architecture

This application was made with DDD approach (or at least - trying to :) ).

## Event Storming
First step was to make event storming. That helped a lot to think and get to know what is needed to be done. Below is the result of simple, one-man meeting. I used https://miro.com for that.

![description](./docs/description.png)

I found out 2 contexts for this use case:
1. Show Management - for managing screening rooms and movie shows

![show-management](./docs/show-management.png)

3. Movie Catalog - for keeping the movie catalog, reviews, etc

![movie-catalog](./docs/movie-catalog.png)

Event storming includes some *conditions* that are not yet implemented (white stickers), but which have impact on the design of the model.

# Code architecture

Codebase is mainly using hexagonal architecture with command and query model separation. That enabled me to firstly focus only
on the business logic, and then just simply implement database adapters and rest controllers.

Each context is divided into 5 main packages:
* deepmodel - where the *deep* domain logic and knowledge exists. That is the place where aggregates are put into.
* process model - place for *processing* domain knowledge - which does not really fit into deep model. That is mainly consisting
of command handlers
* querymodel - place for fetching data
* infrastructure - for integration with external services and technical stuff
* web - for exposing web interfaces

Tests consist of unit and integration tests. While building this app, unit tests are executed at first, and only if all of them passed, integration tests are executed.
This approach requires all unit tests to be named with suffix **-Test**, and integration tests to be named with suffix **-IntegrationTest**.

## How further development will look like

1. While deep model and process model is tested quite well, some unit and integration tests are missing for rest of the packages. However, I made a couple of example tests to show my approach of testing such functionalities.
2. Some functionalities are not exposed by any port/interface yet - eg. activation and deactivation of the movie. That also should be done as the next steps.
3. Maybe definition of "active" movie would also change? For example there will be period of activity of the movie, which will show when shows for that movie can be created. Maybe it should be placed only in the **show management** context?
4. From the business perspective, there is a risk that this design is not really optimal for all use cases. For example - for show management, there should be longer discussion about how the booking process of the tickets will look like,
because that can have a huge impact on the final design.
5. Users shouldn't be (obviously) mocked. However, this should be only requiring to change ```JwtUserDetailsService``` implementation
6. As this app was developed locally - repository does not include any CI/CD configuration. In the "real world" that should be included.
7. Generic, result parameter **out U** should be removed from CommandHandler interface, and then, generic CommandBus should be introduced. Then, it will not be required to autowire
each command handler for each command in the class (ex. **ShowTimeManagementController**), just one CommandBus will be enough.
8. Error handling should be discussed because not all possible exceptions are processed properly (for example, check *MovieCatalogExceptionHandler* - NotFound exception is translated to 404 code).
9. It should be also discussed how we should behave when external services are down - should we just try every time, reduce timeout, or maybe circuit breaker is needed?

## Why there is no commit history?

Well... I fell a little by ashamed of that... but... I developed whole app locally, without pushing it to any remote repository. 
Finally, after completing everything, I just simply wanted to add git remote/origin to gitlab, and, I don't know why, but I just executed
```rm -rf .git```
to simply "prepare clean git repo" for new repository. And then it came up to my mind what I have done, and that all git history is gone.
Luckily, IntelliJ cached whole git commit history, which I attach below. But still, changes between commits are gone :(. Lessons learned -
push everything to the cloud, even if that is just simple challenge!
![git-history](./docs/commithistory.png)
