#!/bin/bash
export PGPASSWORD='FURIOUSOMATIC_DB'
echo "Starting to init db"
docker container exec -i $(docker-compose ps -q furiousomatic-postgres) psql -U FURIOUSOMATIC_DB FURIOUSOMATIC_DB < init-db.sql
echo "Init db finished"
